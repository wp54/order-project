import { IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(2, 50)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
