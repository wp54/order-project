import { IsNotEmpty, IsPositive, Length} from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 16)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  age: number;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
